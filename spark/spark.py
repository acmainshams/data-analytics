from pyspark.sql import SparkSession
from pyspark.sql.functions import avg

# Initialize SparkSession
spark = SparkSession.builder \
    .appName("DataProcessing") \
    .getOrCreate()

# Load data from a CSV file
df = spark.read.csv("/content/gold_prices.csv", header=True, inferSchema=True)

# Show the first few rows of the data
print("Initial Data:")
#df.show()
df.select("Open").show()
# # Filter employees older than 30
older_than_30 = df.filter(df["Open"] > 277.0)
print("Employees older than 30:")
older_than_30.show()

# # Calculate average salary by job title
# average_salary = df.groupBy("job_title").agg(avg("salary").alias("average_salary"))
# print("Average salary by job title:")
# average_salary.show()

# # Use Spark SQL to find high-earning employees
df.createOrReplaceTempView("gold")
high_earning_employees = spark.sql("SELECT * FROM gold ")
print("Employees earning more than 5000:")
high_earning_employees.show()

# # Stop the Spark session
spark.stop()
